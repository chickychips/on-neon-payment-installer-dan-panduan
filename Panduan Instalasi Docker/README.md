# Instalasi Docker Windows

![](./assets/step-1.png)

Download docker for windows di url https://www.docker.com/products/docker-desktop
pilih dan sesuaikan host OS yang ingin kita pakai, dalam ini kita menggunakan Windows
maka pilihlah opsi windows.

![](./assets/step-2.png)

Tunggu sampai proses download selesai

![](./assets/step-3.png)

Ketika sudah selesai, maka kita buka lokasi tempat dimana kita melakukan download
file tersebut, double klik untuk memulai proses intalasi.

![](./assets/step-4.png)

Tunggu prompt instalasi

![](./assets/step-5.png)

Checklist opsi di atas, lalu klik ok untuk memulai proses instalasi.

![](./assets/step-6.png)

Tunggu proses intalasi selesai

![](./assets/step-7.png)

Restart ulang diperlukan untuk mendapatkan effek dari proses instalasi, klik 
tombol close and restart

![](./assets/step-8.png)

Setujui EULA penggunaan aplikasi

![](./assets/step-9.png)

Karena untuk penggunan docker windows perlu mengaktifkan WSL2 (Windows Subsystem Linux)
kita perlu melakukan instalasi WSL Package terlebih dahulu. WSL2 dapat di download
pada link https://aka.ms/wsl2kernel.

![](./assets/step-10.png)

Apabila sudah berhasil di download lakukan proses instalasi WSL2 Engine, lakukan
restart laptop apabila sudah melakukan instlasi WSL2 Engine.

![](./assets/step-11.png)

Ketika pertama kali instalasi, docker desktop akan automatis start. Ini bisa dicek
di notification tray pada taskbar. Apabila terdapat logo tersebut berarti docker 
desktop masih dapat keaadan starting

![](./assets/step-12.png)

Apabila status docker tray sudah seperti gambar di atas, maka docker desktop sudah
dapat digunakan.

![](./assets/step-13.png)

Optional step buka preference pada docker tray, lalu di bagian tab general

- Uncheck `start docker desktop when you log in`, hal ini kita perlukan untuk 
  melakukan automatis start apabila laptop kita sedang baru pertama kali hidup
- Check `use docker compose v2`, hal ini digunakan untuk mengaktifkan integrated
  docker compose command di docker bin.

Ketik tombol apply dan restart

![](./assets/step-14.png)

Buka powershell untuk memastikan docker sudah berhasil running, ketikkan perintah
`docker`. Akan berhasil kalau command output yanh dihasilkan seperti gambar di atas.

![](./assets/step-15.png)

Sama seperti perintah di atas, namun kali ini kita menggunakan command `docker compose`.
Akan berhasil kalau command output yanh dihasilkan seperti gambar di atas.

`docker compose` merupakan fitur baru yang terdapat pada docker desktop sebagai 
integrasi `docker-compose` yang sebelumnya terpisah dari instalasi docker.

![](./assets/step-16.png)

Buka visual studio code,lalu buatfile dengan nama `docker-compose.yaml`, kita isikan 
content seperti dibawah ini:

```yaml
version: "3"
services:
  sqlserver:
    image: mcr.microsoft.com/mssql/server:2019-latest
    environment:
      ACCEPT_EULA: "y"
      SA_PASSWORD: "d4010470d2284913"
      MSSQL_PID: Express
    ports:
      - 1433:1433
    volumes:
      - ./storage:/var/opt/mssql/data
```

Lakukan pull container image

$ docker compose pull 

Run service

$ docker compose up

Atau gunakan perintah berikut ini apabila ingin menstart as background task

$ docker compose -d

Cek apakah service berhasil up

$ docker ps -a

![](./assets/step-19.png)

Akses ke sql server

```
host: localhost
port: 1433
username: sa
password: d4010470d2284913
```