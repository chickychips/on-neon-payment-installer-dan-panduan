# ON NEON Payment Installer dan Panduan

OCBC NISP NEON Batch 4 (Payment)
List installer dan panduan instalasi

|             **Software**            |      **Version**     | **Platform** |      **Keterangan**      |                                      **Link**                                      |
|:-----------------------------------:|:--------------------:|:------------:|:------------------------:|:----------------------------------------------------------------------------------:|
| Visual Studio Code                  | latest               | Windows      | Download dari internet   | https://code.visualstudio.com/                                                     |
| Visual Studio                       | 2022 Community       | Windows      | Download dari internet   | https://my.visualstudio.com/Downloads?q=visual%20studio%202017                     |
| SQL Server                          | 2019  (Docker Image) | Windows      | Download dari docker hub | https://hub.docker.com/_/microsoft-mssql-server                                    |
| .Net 6                              | 6                    | Windows      | Download dari internet   | https://my.visualstudio.com/Downloads?q=visual%20studio%202017                     |
| Node.JS & NPM                       | latest               | Windows      | Download dari internet   | https://nodejs.org/en/download/                                                    |
| Git                                 | latest               | Windows      | Download dari internet   | https://git-scm.com/downloads                                                      |
| SQL Server Management Studio (SSMS) | 18.10                | Windows      | Download dari internet   | https://docs.microsoft.com/en-us/sql/ssms/release-notes-ssms?view=sql-server-ver15 |
| Chrome                              | latest               | Windows      | Download dari internet   | https://www.google.com/chrome/                                                     |
| Docker Desktop                      | latest               | Windows      | Download dari internet   | https://www.docker.com/products/docker-desktop                                     |
